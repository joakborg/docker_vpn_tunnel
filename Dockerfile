FROM ubuntu:20.04

RUN apt-get update \
    && apt-get install -y openconnect=8.05-1 \
    && apt-get install -y curl \
    && apt-get install -y gconf2 \
    && apt-get install -y tinyproxy \
    && apt-get install -y dante-server

COPY resources/csd-post.sh /
COPY resources/docker-entrypoint.sh /
COPY resources/tinyproxy.conf /etc/tinyproxy/
COPY resources/danted.conf /etc/

RUN chmod +x csd-post.sh
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]

EXPOSE 8888
