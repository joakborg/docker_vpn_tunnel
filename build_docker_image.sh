#!/bin/bash

if [ -n "$1" ];
then
  p=$1
else
  p=.
fi

docker build -t cisco_tunnel $p
