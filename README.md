# README #

Dockeriserad lokal VPN-proxy. Nu även med stöd för RDP!

## Vad är den här bra för? ##

Den gör så att du kan peka saker som Postman/Rider/VSCode/etc, mot en lokal proxy som är VPN-uppkopplad, så du inte behöver ha hela ditt nätverksinterface shanghajat av Cisco AnyConnect.

Du kan även köra remote desktop mot maskiner bakom VPN.

## Ok, cool! Hur sätter jag upp den? ##

* Klona det här repot.
* För valfritt linux-skal med docker installerat:
    * Exekvera först `build_docker_image.sh` för att bygga docker imagen.
    * Exekvera sen `run_vpn_in_docker.sh` för att starta en container.
* För powershell:
    * Exekvera först `build_docker_image.ps1` för att bygga docker imagen.
    * Exekvera sen `run_vpn_in_docker.ps1` för att starta en container.
* När den promptar om det, ange användarnamn och sen lösenord (authenticator-lösenord + pin).

Grattis! Proxyn är nu uppe och vpn-kopplad, du kan nu peka om valfria applikationer mot en lokal http-proxy på `127.0.0.1:8888`, alternativt en socks5-proxy på `127.0.0.1:7777`.

## Hur kör jag remote desktop mot maskiner bakom VPN?

Om din RDP-klient stödjer proxy är det bara att ange proxy-inställningarna från punkten ovan för antingen http-proxy eller socks5-proxy.

Om den inte stödjer proxy kan man forcera det genom proxychains (windows-binärer tillgängliga i detta git-repo).

Man skapar då en genväg (för att göra det enklare framgent) vars `target` pekas mot proxychains-binären med argumentet `-f` följt av pathen till proxychains config-fil (finns inkluderad i repot, det viktiga i den är att den pekar mot socks5-proxyn på rätt port) och följt av pathen till sin RDP-klient.

Exempel på `target` för att köra proxychains från C:\proxychains och mRemoteNG från sin default installations-mapp: `C:\proxychains\proxychains_win32_x64.exe -f "C:\proxychains\proxychains.conf" "C:\Program Files (x86)\mRemoteNG\mRemoteNG.exe"`

## Kan jag köra det här på min frukt-produkt? ##

Antagligen?
Bygg- och kör-scripten kanske inte vill lira rakt av, men de är enkla nog att köra manuellt eller modifiera ifall det behövs. (spontant borde de fungera rakt upp-och-ned dock, har ingen frukt-produkt så kan inte verifiera)

## Troubleshooting

### standard_init_linux.go:190: exec user process caused "no such file or directory"

Detta fel är sannolikt orsakat av att din GIT är konfigurerad att "Checkout Windows-style" (core.autocrlf=true) då detta är default på Windows. 

Det gör att repots filers ELS blir CRLF istället för LF, och då blir dockercontainerns filsystem (Linux) ledsen i ögat när den kör igång.

Ändra till "Checkout as-is" så filerna inte automatiskt ändras till CRLF. 

Notera att denna förändring kommer gälla för alla dina GIT repos.

## Är té bättre än kaffe? ##

Jag är glad att du frågade.
Ja, det är det, många gånger om.
