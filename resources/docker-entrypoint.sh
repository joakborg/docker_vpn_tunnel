#!/bin/bash

service tinyproxy start
openconnect --csd-wrapper=/csd-post.sh --background https://anslut.sl.se --os=win
sleep 2 # Sleep behövs för att nätverksinterfacet tun0 ska hinna registreras av openconnect
service danted start
tail -f /var/log/tinyproxy/tinyproxy.log
